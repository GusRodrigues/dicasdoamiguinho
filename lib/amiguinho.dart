import 'dart:math';

import 'package:flutter/material.dart';

class Amiguinho extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'Eu sou o amiguinho',
        ),
      ),
      body: Amigo(),
    );
  }
}

class Amigo extends StatefulWidget {
  @override
  _AmigoState createState() => _AmigoState();
}

class _AmigoState extends State<Amigo> {
  bool _visible = false;
  @override
  Widget build(BuildContext context) {
    return Center(
      child: GestureDetector(
        onTap: () {
          setState(() {
            _visible = true;
          });
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _motivationalText(),
            Image.asset('images/amiguinho.png'),
            SizedBox(height: 15),
          ],
        ),
      ),
    );
  }

  Widget _motivationalText() {
    return AnimatedOpacity(
      duration: Duration(seconds: 1),
      opacity: _visible ? 1.0 : 0.0,
      child: Text(
        _giveATips(),
        style: TextStyle(
          color: Colors.white,
          fontSize: 26.0,
          fontWeight: FontWeight.bold,
          shadows: <Shadow>[
            Shadow(
              color: Colors.black,
              offset: Offset(2, 2),
            ),
            Shadow(
              color: Colors.black,
              offset: Offset(2, -2),
            ),
            Shadow(
              color: Colors.black,
              offset: Offset(-2, 2),
            ),
            Shadow(
              color: Colors.black,
              offset: Offset(-2, -2),
            ),
          ],
        ),
      ),
    );
  }

  String _giveATips() {
    List<String> _tips = [
      'Hidrate-se',
      'Acredite no seu potencial',
      'Pratique esportes',
      'Obedeça seus pais',
      'Recolha o lixo',
      'Arrume o seu quarto',
    ];
    return _tips.elementAt(Random().nextInt(_tips.length));
  }
}
