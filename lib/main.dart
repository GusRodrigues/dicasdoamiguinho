import 'package:flutter/material.dart';
import 'amiguinho.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Amiguinho(),
      // defining the theme.
      theme: ThemeData(
        accentColor: Colors.teal[400],
        primaryColor: Colors.green[600],
        backgroundColor: Colors.white,
      ),
    );
  }
}
